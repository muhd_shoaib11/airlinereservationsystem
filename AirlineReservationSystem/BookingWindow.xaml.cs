﻿using System;
using System.Windows.Controls;

namespace AirlineReservationSystem
{
    /// <summary>
    /// Interaction logic for BookingWindow.xaml
    /// </summary>
    public partial class BookingWindow : Page
    {
        Customer_Hibernate customer;

        public BookingWindow()
        {
            InitializeComponent();
        }
        public BookingWindow(Customer_Hibernate cust)
        {
            InitializeComponent();
            this.CustomerID.Text = cust.Id.ToString();
            this.customer = cust;
        }

        private void SaveBooking(object sender, EventArgs args)
        {
            /*
            int id = Convert.ToInt32(Id.Text);
            int customerID = Convert.ToInt32(CustomerID.Text);
            string flightNo = FlightNo.Text;
            string origin = Origin.Text;
            string destination = Destination.Text;
            string departure = Departure.SelectedDate.Value.ToShortDateString();
            string arrival = Arrival.SelectedDate.Value.ToShortDateString();
            Booking_Hibernate booking = new Booking_Hibernate { Id = id, CustomerID = customerID, FlightNo = flightNo, Destination = destination, Arrival = arrival, Origin = origin, Departure = departure };
            customer.AddBooking(booking);
            var tx = Hibernate.session.BeginTransaction();
            Hibernate.session.Save(booking);
            tx.Commit();
            */
            customer.get();
        }

    }
}
