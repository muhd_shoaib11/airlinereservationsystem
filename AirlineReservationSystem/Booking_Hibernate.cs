﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineReservationSystem
{
    public class Booking_Hibernate
    {
        virtual public int Id { get; set; }
        virtual public int CustomerID { get; set; }
        virtual public string FlightNo { get; set; }
        virtual public string Destination { get; set; }
        virtual public string Origin { get; set; }
        virtual public string Departure { get; set; }
        virtual public string Arrival { get; set; }

        public virtual Customer_Hibernate customer { get; set; }

    }
}
