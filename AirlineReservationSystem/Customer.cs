﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows;

namespace AirlineReservationSystem
{
    class Customer : INotifyPropertyChanged
    {
        public static IList customerData = new List<Customer_Hibernate>();
        public event PropertyChangedEventHandler PropertyChanged;
        public static DataTable customerTable = new DataTable();
        private static SqlDataAdapter sda;
        /*
        int id;
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Id"));
                    MessageBox.Show("Updated Customer Detail" + "\n\n" + 
                        "Name:" + this.Name + "\n" + "ID:" + this.Id);
                }
            }
        }
        */
        public static ObservableCollection<Customer> customers = new ObservableCollection<Customer>();
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string FrequentFlyer { get; set; }
        public string DateOfBirth { get; set; }
        public string FavouriteAirline { get; set; }

        public void add(int id, string name, string address, string frequentFlyer, string dateOfBirth, string favouriteAirline)
        {
            this.Id = id;
            this.Name = name;
            this.Address = address;
            this.FrequentFlyer = frequentFlyer;
            this.DateOfBirth = dateOfBirth;
            this.FavouriteAirline = favouriteAirline;
            customers.Add(this);
        }

        public void addToTable(int id, string name, string address, string frequentFlyer, string dateOfBirth, string favouriteAirline)
        {
            try
            {
                customerTable.Rows.Add(id, name, address, frequentFlyer, DateTime.Parse(dateOfBirth), favouriteAirline);
                updateDB();
            }
            catch (System.Data.ConstraintException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void createTable()
        {
            customerTable.Columns.Add("Id", typeof(int));
            customerTable.Columns.Add("Name", typeof(string));
            customerTable.Columns.Add("Address", typeof(string));
            customerTable.Columns.Add("FrequentFlyer", typeof(string));
            customerTable.Columns.Add("DateOfBirth", typeof(DateTime));
            customerTable.Columns.Add("FavouriteAirline", typeof(string));
            customerTable.PrimaryKey = new DataColumn[] { customerTable.Columns["Id"] };

            customerTable.AcceptChanges();
            customerTable.RowChanged += new DataRowChangeEventHandler(Row_Changed);
        }

        private static void Row_Changed(object sender, DataRowChangeEventArgs e)
        {
            string customerDetails = "";
            foreach (DataRow r in customerTable.Rows)
            {
                customerDetails += r["Id"] + " " + r["Name"] + " " + r["Address"] + " " + r["FrequentFlyer"] + " " + r["DateOfBirth"] + " " + r["FavouriteAirline"] + "\n";
            }
            MessageBox.Show(customerDetails);
            updateDB();
        }

        public static void getData()
        {
            String hql = "FROM Customer_Hibernate";
            var query = Hibernate.session.CreateQuery(hql);
            customerData = query.List();
        }

        private static void Column_Changed(object sender, DataColumnChangeEventArgs e)
        {
            updateDB();
            string customerDetails = "";
            foreach (DataRow r in customerTable.Rows)
            {
                customerDetails += r["Id"] + " " + r["Name"] + " " + r["Address"] + " " + r["FrequentFlyer"] + " " + r["DateOfBirth"] + " " + r["FavouriteAirline"] + "\n";
            }
            MessageBox.Show(customerDetails);
        }

        private static void updateDB()
        {
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);
            sda.Update(customerTable);
        }
    }
}