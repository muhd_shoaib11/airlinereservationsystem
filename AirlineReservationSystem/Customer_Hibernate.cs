﻿using NHibernate.Criterion;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace AirlineReservationSystem
{
    public class Customer_Hibernate : INotifyPropertyChanged
    {
        virtual public event PropertyChangedEventHandler PropertyChanged;
        int id;
        virtual public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Id"));
                    this.updateDB_HQL();
                    //this.updateDB_Criteria();
                }
            }
        }
        virtual public string Name { get; set; }
        virtual public string Address { get; set; }
        virtual public string FrequentFlyer { get; set; }
        virtual public string DateOfBirth { get; set; }
        virtual public string FavouriteAirline { get; set; }

        public static IList customerData = new List<Customer_Hibernate>();
        public static void getData_HQL()
        {
            String hql = "FROM Customer_Hibernate";
            var query = Hibernate.session.CreateQuery(hql);
            customerData = query.List();
        }
        public static void getData_Criteria()
        {
            var criteria = Hibernate.session
            .CreateCriteria<Customer_Hibernate>();
            customerData = criteria.List();
        }
        public static void getFilteredData_HQL()
        {
            String hql = "FROM Customer_Hibernate WHERE FrequentFlyer = 'Yes'";
            var query = Hibernate.session.CreateQuery(hql);
            customerData = query.List();
        }
        public static void getFilteredData_Criteria()
        {
            var criteria = Hibernate.session.CreateCriteria<Customer_Hibernate>();
            customerData = criteria.Add(Restrictions.Eq("FrequentFlyer", "Yes"))
            .List();
        }
        private void updateDB_HQL()
        {
            String hql = "UPDATE Customer_Hibernate set Id=:Id WHERE Name=:Name AND Address =:Address AND FrequentFlyer =:FrequentFlyer AND DateOfBirth =:DateOfBirth AND FavouriteAirline :=FavouriteAirline";
            var query = Hibernate.session.CreateQuery(hql);
            query.SetParameter("Id", this.Id).SetParameter("Name", this.Name).SetParameter("Address", this.Address).SetParameter("FrequentFlyer", this.FrequentFlyer).SetParameter("DateOfBirth", DateOfBirth).SetParameter("FavouriteAirline", FavouriteAirline) ;
            query.ExecuteUpdate();
        }
        private void updateDB_Criteria()
        {
            var tx = Hibernate.session.BeginTransaction();
            var criteria = Hibernate.session.CreateCriteria<Customer_Hibernate>();
            var customer = criteria.List();
            
            //Customer_Hibernate newCustomer = (Customer_Hibernate)criteria.Add(Restrictions.Eq("Name", this.Name)).Add(Restrictions.Eq("Address", this.Address)).Add(Restrictions.Eq("FrequentFlyer", this.FrequentFlyer)).Add(Restrictions.Eq("DateOfBirth", this.DateOfBirth)).Add((Restrictions.Eq("FavouriteAirline", this.FavouriteAirline))).List()[0];
            //newCustomer.Id = this.Id;
            Hibernate.session.Update(customer);
            tx.Commit();
        }
        public virtual ISet<Booking_Hibernate> Bookings { get; set; }
        public Customer_Hibernate()
        {
            Bookings = new HashSet<Booking_Hibernate>();
        }
        public virtual void AddBooking(Booking_Hibernate booking)
        {
            Bookings.Add(booking); booking.customer = this;
        }

        public virtual void get()
        {
            var customer = Hibernate.session.Get<Customer_Hibernate>(this.Id); //this.Id = 3
        }
    }
}


