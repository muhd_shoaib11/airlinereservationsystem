﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineReservationSystem
{
    class DB
    {
        private static string server = "DESKTOP-4VT98KC";
        private static string instance = "SQLEXPRESS";
        private static string database = "AirlineReservationSystem";
        private static string connString = "Integrated Security=true" + ";" + "database=" + database + ";" + "server=" + server + "\\" + instance;
        public static SqlConnection conn;
        static DB()
        {
            conn = new SqlConnection(connString);
            conn.Open();
        }

        public static Dictionary<string, List<(string, int)>> index = new Dictionary<string, List<(string, int)>>();
        public static void createIndex()
        {
            string table_name = "Customer";
            string query = "SELECT * FROM " + table_name;
            SqlCommand command = new SqlCommand(query, conn);
            SqlDataReader reader = command.ExecuteReader();
            DataTable table = new DataTable();
            table.Load(reader);
            for (int row_id = 1; row_id <= table.Rows.Count; row_id++)
            {
                DataRow row = table.Rows[row_id-1];
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    string key = Convert.ToString(row[i]);
                    if (index.ContainsKey(key))
                    {
                        List<(string, int)> value = index[key];
                        value.Add((table_name, row_id));
                    }
                    else
                    {
                        index.Add(key, new List<(string, int)> { (table_name, row_id) });
                    }
                }
            }
        }

    }
}
