﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace AirlineReservationSystem
{

    public partial class MainWindow : Page
    {

        public MainWindow()
        {
            InitializeComponent();
            //dataGrid.ItemsSource = Customer.customers;
            //Customer_Hibernate.getFilteredData_HQL(); //or Customer_Hibernate.getData_Criteria();
            //dataGrid.ItemsSource = Customer_Hibernate.customerData;
            DB.createIndex();
            

        }

        public MainWindow(string str) {
            InitializeComponent();
            dataGrid.ItemsSource = Customer.customerTable.DefaultView;
        }

        void NavigateToBookingWindow(object sender, RoutedEventArgs e)
        {
            //DataRowView selectedItem = (DataRowView)dataGrid.SelectedItem;
            
            Customer_Hibernate customer = (Customer_Hibernate)dataGrid.SelectedItem;
            if (customer == null)
            {
                MessageBox.Show("You must select a row!");
            }
            else
            {
                BookingWindow bw = new BookingWindow(customer);
                this.NavigationService.Navigate(bw);
            }
        }

        private void textChangedEventHandler(object sender, EventArgs args)
        {
            TextBox tb = (TextBox)sender;
            if (!tb.Text.Equals(""))
            {
                tb.Background = new SolidColorBrush(Colors.Beige);
            }
            else
            {
                tb.Background = new SolidColorBrush(Colors.White);
            }
        }

        private void buttonCheckedEventHandler(object sender, EventArgs args)
        {
            RadioButton rb = (RadioButton)sender;
            MessageBox.Show("You are " + rb.Content);
        }

        private void selectionChangedEventHandler(object sender, SelectionChangedEventArgs args)
        {
            ComboBoxItem item = (ComboBoxItem)args.AddedItems[0];
            if(item.Content.ToString().Equals("Lufthansa"))
            {
                MessageBox.Show("Lufthansa is the largest German airline.");
            }
            else if (item.Content.ToString().Equals("Emirates"))
            {
                MessageBox.Show("Emirates is an airline based in Dubai, United Arab Emirates.");
            }
            else
            {
                MessageBox.Show("Qantas Airways is the flag carrier of Australia.");
            }
        }

        private void validateForm(object sender,  EventArgs args)
        {
            String input = ((TextBox)grid.FindName("id")).Text;
            int id = 0;
            if (input.Equals(""))
            {
                MessageBox.Show("ID must be provided!");
            }
            else
            {
                try
                {
                    id = Convert.ToInt32(input);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error: ID should be numeric!");
                }
            }

            input = ((TextBox)grid.FindName("name")).Text;
            String name = "";
            if (input.Equals(""))
            {
                MessageBox.Show("Name must be provided!");
            }
            else
            {
                if (Regex.Match(input, "^[a-zA-Z\\s]+$").Success)
                {
                    name = input;
                }
                else
                {
                    MessageBox.Show("Error: Name should contain only letters and/or spaces!");
                }
            }

            string address = ((TextBox)grid.FindName("address")).Text;

            string frequentFlyer = ((ListBoxItem)freqFlyer.SelectedItem).Content.ToString();

            string dateOfBirth = ((DatePicker)grid.FindName("dateOfBirth")).SelectedDate.Value.ToShortDateString();

            string favouriteAirline = ((ComboBoxItem)favAirline.SelectedItem).Content.ToString();
            Customer customer = new Customer();
            customer.add(id, name, address, frequentFlyer, dateOfBirth, favouriteAirline);
            //dataGrid.DataContext = Customer.customers;

            /*
            Customer_Hibernate customer = new Customer_Hibernate { Id = id, Name = name, Address = address, FrequentFlyer = frequentFlyer, DateOfBirth = dateOfBirth, FavouriteAirline = favouriteAirline };
            ITransaction transaction = Hibernate.session.BeginTransaction();
            Hibernate.session.Save(customer);
            transaction.Commit();
            IList customers = Hibernate.session.CreateCriteria<Customer_Hibernate>().List();
            */

        }

        private void DataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine(e.Key);
            if (e.Key == Key.Back)
            {
                Console.WriteLine("1");
            }


        }
    }
}
